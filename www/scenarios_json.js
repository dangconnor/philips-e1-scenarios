var scenariosObj = {
// START

			"A1": {
			  "scenarioTitle" : "Remote reporting",
			  "specialistJobDescriptions" : [
			  	{ "specialist" : "Dr. Jones (Radiology CT)" },
			  	{ "specialist" : "Dr. Khan (Neuro-radiology Specialist)" },
			  	{ "specialist" : "Dr. Smith (Consultant Radiology)" }
			  ],
			  "events" : [
			    {
			      "eventTitle" : "A&amp;E consultant requests CT scan",
			      "eventDetails" : "Request created",
			      "eventIcon" : "icon_CT_scan.png",
			      "eventImage" : ""

			    },
			    {
			      "eventTitle" : "Dr. Jones completes the initial report at home",
			      "eventDetails" : "<ul><li>Dr. Jones has access to Philips Patient History Timeline</li><li>Full diagnostic images available</li><li>Full radiologists toolset over VPN or mobile network</li><li>Access to Philips CT Acute Multi-Functional Review</li><li>Uses Exam Notes to facilitate communication</li></ul>",
			      "eventIcon" : "icon_PC_assessment.png",
			      "eventImage" : ""

			    },
			    {
			      "eventTitle" : "Dr. Khan provides a second opinion via a mobile device",
			      "eventDetails" : "<ul><li>Has access to diagnostic quality images using a feature-rich zero footprint viewer</li><li>Able to view all saved image datasets and the initial report</li></ul>",
			      "eventIcon" : "icon_mobile.png",
			      "eventImage" : ""

			    },
			    {
			      "eventTitle" : "Dr. Jones contacts Dr. Smith who notes a discrepancy in the initial report",
			      "eventDetails" : "<ul><li>Remote access delivers fully functional PACS or RIS driven workflow</li><li>Philips Dynamic Worklist allows instant visibility of urgent studies</li><li>Philips PACS helps deliver GM's goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li><li>Dr. Smith can use Philips ED-RAD to discuss the discrepancy</li><li>Dr. Smith is able to double report the study</li><li>Philips PACS supports instant chat for collaboration</li></ul>",
			      "eventIcon" : "icon_discrepancy.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Dr. Jones reviews the case, adds key images for clinicians and provides the final report",
			      "eventDetails" : "<ul><li>Abnormal variants flagged and saved creating a subset of data – automatically creating key images</li><li>Exam notes can be added along with advanced annotations and measurements</li><li>Using either a PACS or RIS driven workflow, Dr. Jones is able to finalise the report</li></ul>",
			      "eventIcon" : "icon_review.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Report is available in PACS/RIS and EPR",
			      "eventDetails" : "<ul><li>Philips supports interfacing with multiple upstream system for report distribution</li><li>Philips PACS users are able to see the report and automatic display of the key images</li></ul>",
			      "eventIcon" : "icon_report_available.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "The referring clinician is alerted to the urgent results",
			      "eventDetails" : "<ul><li>Philips ED-RAD keeps the clinicians informed of progress</li><li>HSS CRIS Communicator works on a closed loop system with multiple escalation paths</li></ul>",
			      "eventIcon" : "icon_alert.png",
			      "eventImage" : ""
			    }
			  ]
			},



			"A2": {
			  "scenarioTitle" : "Cross-site Reporting",
			  "specialistJobDescriptions" : [
			  	{ "specialist" : "Referring Paediatrician – requests MR scan (Bolton)" },
			  	{ "specialist" : "Radiographer - performs MR scan (Bolton)" },
			  	{ "specialist" : "Consultant Paediatric Radiologist (Manchester Children's Hospital)" }
			  ],
			  "events" : [
			    {
			      "eventTitle" : "Jack referred for an urgent MR scan at Bolton A&E",
			      "eventDetails" : "Request urgent MR scan",
			      "eventIcon" : "icon_MR_scan.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Radiographer undertakes scan at Bolton and identifies urgent pathology",
			      "eventDetails" : "<ul><li>In Philips PACS the radiographer can add a critical exam note and select key images to indicate the pathology they have identified on the scan</li><li>The radiographer assigns the study to the consultant radiologist at the Children's Hospital</li><li>Philips PACS supports instant chat for collaboration</li></ul>",
			      "eventIcon" : "icon_identify.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Consultant Radiologist at Manchester reports the scan using RIS based reporting with VR",
			      "eventDetails" : "<ul><li>Philips \"Organisational Linking\" delivers the single Philips Patient History Timeline across the GM collaborative</li><li>Philips PACS helps achieve GM's goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li><li>Philips integrates with Active Directory delivering Role Based Access Control, ensuring legitimate, auditable access to patient data is secure</li><li>Philips PACS supports both RIS and PACS driven workflow</li><li>Philips Dynamic Worklists allows instant visibility of urgent studies across the GM collaborative</li><li>Philips Key Image function directs the radiologist to the pathology</li><li>Philips clinical applications are launched automatically as part of the hanging protocols</li></ul>",
			      "eventIcon" : "icon_report_available.png",
			      "eventImage" : ""
			    }
			  ]
			},



			"B": {
			  "scenarioTitle" : "Mammography",
			  "specialistJobDescriptions" : [
			  	{ "specialist" : "Breast Screening Radiographer (Mobile Unit)" },
			  	{ "specialist" : "Breast Screening Radiologist (USMH)" },
			  	{ "specialist" : "Screening Assessment Radiographer - Tomosynthesis Imaging" },
					{ "specialist" : "Consultant Surgeon" },
					{ "specialist" : "Dr. White (Consultant Radiologist)" }
			  ],
			  "events" : [
			    {
			      "eventTitle" : "Christine has routine breast screening",
			      "eventDetails" : "<ul><li>Philips PACS is in clinical use with NBSS interfaced via HL7</li><li>Images can be uploaded to Philips PACS via a secure direct link</li><li>If there is no direct link the images are saved to a dedicated encrypted hard drive</li><li>Philips Advanced Mammography supports breast screening reporting workflow</li></ul>",
			      "eventIcon" : "icon_breast_screening.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Christine attends the Nightingale screening assessment clinic and has tomosynthesis imaging",
			      "eventDetails" : "<ul><li>Philips Advanced Mammography supports tomosynthesis imaging</li><li>Philips Advanced Mammography streamlines workflow allowing screening and tomosynthesis images to be accessed from the Philips Patient History Timeline</li><li>Philips Advanced Mammography combines the review speed, functionality and quality of a dedicated mammography workstation</li></ul>",
			      "eventIcon" : "icon_imaging.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Christine attends symptomatic breast clinic and has further imaging",
			      "eventDetails" : "<ul><li>Philips \"Organisational Linking\" delivers the single Philips Patient History Timeline across the GM collaborative</li><li>Philips PACS offers Advanced Mammography workflow</li><li>Philips PACS allows screening, tomosynthesis, symptomatic and all other images to be displayed in the single Patient History Timeline</li><li>Certified to support NBSS Phase 2 - Philips PACS provides the efficiency of integrated workflow</li><li>Philips PACS helps deliver GM's goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li></ul>",
			      "eventIcon" : "icon_imaging.png",
			      "eventImage" : ""
			    },
				{
					"eventTitle" : "Christine has a consultation with the surgeon",
					"eventDetails" : "<ul><li>The surgeon is able to show Christine all the imaging, pathology and reports via the single Philips Patient History Timeline which includes all her breast imaging</li><li>The surgeon benefits from access to Key Images and Exam Notes to assist his consultation with Christine</li></ul>",
					"eventIcon" : "icon_consultation.png",
					"eventImage" : ""
				},
				{
					"eventTitle" : "Dr. White reports latest imaging",
					"eventDetails" : "Dr. White benefits from the following Philips PACS features:</br><ul><li>Tomosynthesis tools</li><li>Automatic tissue alignment</li><li>Background suppression</li><li>Dedicated keypad and hanging protocols</li><li>Double blind reading</li><li>Bi-directional localiser for synthetic 2D to 3D and 3D to 2D</li><li>DBT slice indicator/slab tool</li><li>Auto scaling and step-zoom behaviour</li></ul>",
					"eventIcon" : "icon_report_available.png",
					"eventImage" : ""
				}
			  ]
			},


			"C": {
			  "scenarioTitle" : "MDT and Teaching",
			  "specialistJobDescriptions" : [
			  	{ "specialist" : "Consultant Radiologist (WWL)" },
			  	{ "specialist" : "Dr. Patel (Consultant Radiologist, UHSM)" },
			  	{ "specialist" : "MDT Coordinator" }
			  ],
			  "events" : [
			    {
			      "eventTitle" : "Mrs. Jones has a CT Colonoscopy",
			      "eventDetails" : "<ul><li>The Philips Zero Click function automatically performs the segmentation of the CT data in preparation for reporting</li><li>Philips CT Virtual Colonoscopy (VC) supports electronic cleansing and CAD</li></ul>",
			      "eventIcon" : "icon_colonoscopy.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "CT Colonscopy is reported at WWL and added to the UHSM MDT",
			      "eventDetails" : "<ul><li>Philips CT VC segmentation is ready for viewing immediately</li><li>Philips CT VC reduces reading times to approximately five-to-ten minutes</li><li>Exclusive to Philips, is the Perspective Filet View which provides a \“virtual dissection\” of the colon</li><li>Conference presentation states are created to include window width/level, zoom, rotate, annotations and measurements</li><li>Philips offers a fully featured, web-based virtual MDT management system</li><li>Cases can be entered to an MDT list either manually or automatically</li><li>Philips PACS helps deliver GM’s goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li></ul>",
			      "eventIcon" : "icon_report_available.png",
			      "eventImage" : ""
			    },
			    {
			      "eventTitle" : "Dr. Patel prepares the case for the UHSM MDT",
			      "eventDetails" : "<ul><li>Philips \"Organisational Linking\" delivers the single Philips Patient History Timeline across the GM collaborative including radiology, medical photography and pathology</li><li>An icon alerts Dr. Patel that previously saved conference presentation states are available</li><li>Dr. Patel will have access to the fully featured, web-based virtual MDT management system</li></ul>",
			      "eventIcon" : "icon_consultation.png",
			      "eventImage" : ""
			    },
				{
					"eventTitle" : "Dr. Patel creates and saves images to the teaching file",
					"eventDetails" : "<ul><li>The web based teaching system has flexible folders, where users create their own hierarchical structure</li><li>Cases and folders can be kept private, shared with all users, or a subset of users</li><li>A single click to anonymise and publish cases to the teaching file repository for sharing</li><li>The system has a black marker tool to permanently change the images and hide any patient identifiable information</li><li>Osirix will be interfaced with the Philips solution via the Application Programme Interface</li></ul>",
					"eventIcon" : "icon_file.png",
					"eventImage" : ""
				},
				{
					"eventTitle" : "During the MDT meeting",
					"eventDetails" : "<ul><li>Philips “Organisational Linking” delivers the single Philips Patient History Timeline across the collaborative including radiology, medical photography and pathology</li><li>The study is opened using saved conference display protocols</li><li>The coordinator can record discussion points, outcomes, request further tests and task assignments</li><li>	Information in the MDT can be automatically communicated to the referring clinician, the patient's GP, the wider care team and the EPR</li></ul>",
					"eventIcon" : "icon_consultation.png",
					"eventImage" : ""
				}
			  ]
			},

			"D1": {
				"scenarioTitle" : "Sensitive Images/Medical Illustration",
				"specialistJobDescriptions" : [
					{ "specialist" : "Radiographer (NMGH)" },
					{ "specialist" : "Medical Photography (NMGH)" },
					{ "specialist" : "Information Governance" },
					{ "specialist" : "General Practitioner" },
					{ "specialist" : "Specific Trust personnel with access rights to Joe's record" }
				],
				"events" : [
					{
						"eventTitle" : "At NMGH Joe has medical photography and radiology imaging",
						"eventDetails" : "<ul><li>Philips \"Organisational Linking\" delivers the single Philips Patient History Timeline across the GM collaborative including radiology and medical photography</li><li>Philips Visible Light Capture allows mobile workers to send images directly to the Philips PACS whilst still adhering to patient privacy policies, providing clinical context and enhanced clinician workflows</li><li>Philips PACS provides application level security, authentication, session management, access control, auditing and data integrity checks</li><li>It manages restrictions by creating a specific referral group where the records will reside. Medical photography has its own privilege sets and can be restricted to specific personnel only</li></ul>",
						"eventIcon" : "icon_radiology.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Information Governance referral made",
						"eventDetails" : "<ul><li>The Philips PACS has very detailed audit trail capabilities which are available in standard formats such as MS Excel</li><li>Every action performed by users of the system is logged</li><li>The user activity on Philips PACS is fully auditable using an in-built audit package (to which access is controlled). The audit reporting models include	Audit by user and	Audit by patient</li></ul>",
						"eventIcon" : "icon_information.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Secure transfer of images to a solicitor",
						"eventDetails" : "<ul><li>Encrypted CD produced</li><li>Password sent separately using NHS email or verified accounts</li><li>An Excel spreadsheet with the audit summary exported from PACS with the activity can also be sent</li><li>The following information is recorded for audit against the DICOM export:	User name of person who initiated the export,	the date/time of export,	export destination</li></ul>",
						"eventIcon" : "icon_secure.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "'Stop' note requested by Joe's mother",
						"eventDetails" : "The solution supports the stop noting action by:</br><ul><li>HSS CRIS supports a flag for patients whose data needs to be restricted</li><li>This flag is sent via HL7 to the Philips PACS prompting a VIP warning message</li></ul>",
						"eventIcon" : "icon_stop.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "12 months later Joe is admitted to Paediatric ward at WWL",
						"eventDetails" : "<ul><li>Philips “Organisational Linking” delivers the single Philips Patient History Timeline across the collaborative including radiology and medical photography</li><li>Joe is set as a VIP case in Philips PACS</li><li>When a user attempts to access a VIP medical record a warning message is displayed to alert the user that Joe’s records are restricted</li></ul>",
						"eventIcon" : "icon_paediatric.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "WWL clinician requires access to closed records",
						"eventDetails" : "<ul><li>Should the user ‘break-glass’ and access the records, an audit log will monitor the user and activity</li><li>The warning message is configurable per Trust to allow for different departmental policies</li></ul>",
						"eventIcon" : "icon_imaging.png",
						"eventImage" : ""
					}
				]
			},

			"D2": {
				"scenarioTitle" : "Community Workflow",
				"specialistJobDescriptions" : [
					{ "specialist" : "Sam (Community Nurse)" }
				],
				"events" : [
					{
						"eventTitle" : "Sam takes medical photographs of Edith's leg",
						"eventDetails" : "<ul><li>Philips VL Capture allows mobile workers to capture images for storage directly into PACS</li><li>Consent can be recorded and uploaded with the image</li><li>Key features of Philips VL Capture are:</li><ul><li>Zero footprint</li><li>Ability to select patient context/visits</li><li>Import of JPEG images</li><li>Ability to edit images (flip/rotate/crop/brightness)</li><li>Ability to associate/annotate body parts</li><li>DICOM conversion</li><li>Secure storage into Philips PACS</li></ul></ul>",
						"eventIcon" : "icon_leg.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Sam uploads the newly acquired medical photographs to Philips PACS",
						"eventDetails" : "<ul><li>Sam can access the VL Capture via an iOS device or digital camera</li><li>Sam can select Edith's details from a worklist or scan a barcode to display Edith's demographic details</li><li>Sam adds annotations, notes the exam code and marks the anatomical area of the ulcer to add clinical context</li><li>Sam sends the photographs to the Philips PACS where they will become part of the overall imaging archive and be present on the Philips Patient History Timeline</li></ul>",
						"eventIcon" : "icon_upload.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Sam accesses Philips PACS to view his assigned medical images",
						"eventDetails" : "<ul><li>Sam's access can be limited to assigned medical photography within PACS by using access rights in the administration tool</li><li>Other clinicians will have access to the full content of Edith's imaging history within the Philips Patient History Timeline</li><li>The level of access is configurable in the easy-to-use PACS Administration Tool</li></ul>",
						"eventIcon" : "icon_PC_assessment.png",
						"eventImage" : ""
					}
				]
			},

			"D3": {
				"scenarioTitle" : "Non-Radiology (Digital Pathology and Medical Photography)",
				"specialistJobDescriptions" : [
					{ "specialist" : "Medical Photographer" },
					{ "specialist" : "Dr. Young - Histopathologist" },
					{ "specialist" : "Dr. Owen - Virtual Pathology Network" }
				],
				"events" : [
					{
						"eventTitle" : "Mr. Smith attends dermatology clinic",
						"eventDetails" : "<ul><li>Philips PACS supports the storage and display of medical imaging including medical photography, dermoscopy and digital images of slides</li><li>Philips \"Organisational Linking\" delivers the single Philips Patient History Timeline across the collaborative including radiology, medical photography and digital images of slides</li></ul>",
						"eventIcon" : "icon_hospital.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Medical photographs are uploaded to PACS",
						"eventDetails" : "<ul><li>Philips VL Capture allows mobile workers to capture images for storage directly into PACS</li><li>This is performed adhering to patient privacy policies, providing clinical context and enhancing clinical workflows</li><li>Consent can be recorded and uploaded with the image</li><li>Demographic information can be associated with the medical images by searching for the patient record to attach it to the images</li></ul>",
						"eventIcon" : "icon_xray.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Report of biopsy is uploaded to LIMS",
						"eventDetails" : "<ul><li>Philips PACS can receive HL7 based reports from external systems such as LIMS</li><li>Subsequently the pathology report will be available from within the single Philips Patient History Timeline across the collaborative</li></ul>",
						"eventIcon" : "icon_biopsy.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Digital images of the slides and dermoscopy images are uploaded to PACS",
						"eventDetails" : "<ul><li>Philips PACS stores and displays the digital images of the slides and the dermoscopy images</li><li>The single Philips Patient History Timeline for Mr. Smith will contain both radiology, and pathology imaging and reports</li></ul>",
						"eventIcon" : "icon_review.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Dr. Owen request additional laboratory work which is uploaded to PACS",
						"eventDetails" : "<ul><li>The new digital images are available within Mr. Smith's Philips Patient History Timeline to facilitate comparison and enterprise wide distribution</li><li>Philips PACS helps deliver GM's goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li></ul>",
						"eventIcon" : "icon_PC_assessment.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Mr. Smith's case is scheduled for the SKIN MDT",
						"eventDetails" : "<ul><li>Philips offers a fully featured, web-based virtual MDT management system</li><li>Cases can be entered to an MDT list either manually via the PACS integration, or they can be automatically added by using key phrases or examination codes</li><li>The system has a black marker tool. This will permanently change the images and hide any patient identifiable information to protect the identity of the patient</li></ul>",
						"eventIcon" : "icon_date.png",
						"eventImage" : ""
					}
				]
			},

			"E": {
				"scenarioTitle" : "A&E Workflow",
				"specialistJobDescriptions" : [
					{ "specialist" : "A&E Radiographer (Tameside)" },
					{ "specialist" : "A&E Registrar (Tameside)" },
					{ "specialist" : "Amanda (Reporting Radiographer)" }
				],
				"events" : [
					{
						"eventTitle" : "A&E Radiographer undertakes plain film imaging",
						"eventDetails" : "<ul><li>The radiographer performs the examination and notices an anomaly</li><li>The radiographer adds a critical exam note to the study in Philips PACS. This note automatically displays when the A&E clinician opens the study</li></ul>",
						"eventIcon" : "icon_xray.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "A&E Registrar reviews the plain films",
						"eventDetails" : "<ul><li>The critical exam note automatically displays when the registrar opens the study</li><li>The registrar is able to add comments using the Philips ED-RAD Discrepancy tool</li><li>Philips ED-RAD Discrepancy provides powerful enhanced clinical workflow to improve radiology workflow, communication and reporting</li></ul>",
						"eventIcon" : "icon_review.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Amanda reports the plain films",
						"eventDetails" : "<ul><li>Philips Dynamic Worklists allow Amanda instant visibility of urgent studies</li><li>To assist Amanda with clinical safety the ED-RAD pop up box will appear and display the preliminary interpretation from the A&E Registrar</li><li>Philips ED-RAD Discrepancy is able to improve inter-departmental communication and enhance patient care</li><li>Amanda can agree or disagree with the preliminary interpretation and add comments</li><li>Amanda’s findings are available in the ED-RAD dashboard. This allows the A&E Registrar to acknowledge and close the discrepancy or add further comments</li><li>Philips ED-RAD Discrepancy offers ‘real time’ visibility via the dashboard for A&E and Radiology to view discrepancies awaiting acknowledgement</li></ul>",
						"eventIcon" : "icon_checklist.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "Orthopaedic Consultant requires access to previous MR scan",
						"eventDetails" : "<ul><li>An MR study from 3 months ago was acquired outside the collaborative</li><li>Philips PACS provides an Image Exchange Portal ‘ORG’ (partition) which is specifically created for imaging acquired outside the collaborative, showing any previous scans</li><li>The ‘ORG’ allows the consultant to access the previous MR scan</li><li>The ‘ORG’ has a temporary cache which allows studies to delete after a defined period</li></ul>",
						"eventIcon" : "icon_MR_scan.png",
						"eventImage" : ""
					}
				]
			},

			"F": {
				"scenarioTitle" : "ONCOLOGY, SURGICAL PLANNING AND REPORTING EFFICIENCY",
				"specialistJobDescriptions" : [
					{ "specialist" : "Radiologist (Pennine)" },
					{ "specialist" : "Radiologist (Christie Cancer Centre)" },
					{ "specialist" : "Clinicians" }
				],
				"events" : [
					{
						"eventTitle" : "CT scan performed and reported at Pennine",
						"eventDetails" : "<ul><li>Philips PACS helps deliver GM’s goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li><li>Philips Dynamic Worklists allow instant visibility of studies waiting to be reported, allowing efficient prioritisation</li><li>Images with measurements and annotations are automatically flagged as Key Images</li><li>Philips clinical applications support a ‘zero click’ workflow providing automated processing without user interaction</li><li>Algorithms are able to learn the radiologists’ pre-processing patterns and anticipate their preference before a case is opened, enabling reduced reporting time and automatic advanced processing</li><li>Comprehensive automated hanging protocols facilitate auto-linking between and across studies, included are MPR, (m)MIP, volume rendering etc.</li><li>Users can create their own specific hanging protocols and they will be trained how to do this</li><li>Philips PACS has 'Hanging Protocol Sequences'. These can be saved against a study and used to sequence through a set of hangings</li></ul>",
						"eventIcon" : "icon_CT2.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "CT scan performed at Pennine is reviewed at the Christie",
						"eventDetails" : "<ul><li>Philips \"Organisational Linking\" delivers the single Philips Patient History Timeline across the collaborative including radiology, reports and measurements</li><li>Christies have access to all of Norman's previous studies regardless of where they were acquired</li><li>Key Images support the oncology workflow and Philips PACS can automatically create Key Images</li><li>Measurements of an area of interest or annotation will automatically generate a Key Image improving the reporting workflow and ensuring referrers are alerted</li><li>The user can access a direct view of only Key Images if required</li></ul>",
						"eventIcon" : "icon_group_review.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "During treatment a follow-up CT scan is performed",
						"eventDetails" : "<ul><li>Radiologists at Pennine and Christies make use of the advanced Philips Multi-Modality Tumour Tracking (MMTT) application</li><li>MMTT provides automatic and manual standardised measurements of tumour progression over time, including burden calculation based on WHO, RECIST1.0, RECIST 1.1, CHOI, IRRC, mRECIST & PERCIST</li><li>MMTT allows radiologists to use CT, MR, PET/CT, and SPECT/CT data to monitor disease state and assess treatment response</li><li>MMTT supports manual or automatic fusion across modalities such as PET-CT, SPECT-CT, NM-CT, CT-CT, MR-MR and CT-MR</li></ul>",
						"eventIcon" : "icon_CT2.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "The clinician views the CT scan report and measurements in EPR",
						"eventDetails" : "<ul><li>The resultant tables from the MMTT application can be copied into the report on CRIS or stored back to the PACS together with the Key Images, results and graphs which have tracked the tumour. These are available for viewing from within the EPR</li><li>Philips PACS can be integrated with multiple EPR systems using the Philips Application Programme Interface and Software Development Toolkit</li><li>URL Based Integration (SUBI) is the standard usage of the API for EPR integrations</li></ul>",
						"eventIcon" : "icon_checks.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "The most recent CT scan is reported",
						"eventDetails" : "<ul><li>Philips \“Organisational Linking\” delivers the single Philips Patient History Timeline across the GM collaborative including radiology, reports and measurements</li><li>MMTT provides automatic and manual standardised measurements of tumour progression over time, including burden calculation based on WHO, RECIST1.0, RECIST 1.1,  CHOI, IRRC, mRECIST & PERCIST</li><li>The resultant tables from the MMTT application can be copied into the report on CRIS or stored back to the PACS together with the Key Images, results and graphs which have tracked the tumour. These are available for viewing from within the EPR</li></ul>",
						"eventIcon" : "icon_CT_scan.png",
						"eventImage" : ""
					}
				]
			},

			"G": {
				"scenarioTitle" : "Major Incident",
				"specialistJobDescriptions" : [
					{ "specialist" : "Vascular Surgeon (UHSM)" },
					{ "specialist" : "PACS System Administrator" }
				],
				"events" : [
					{
						"eventTitle" : "Vascular Surgeon at UHSM reviews a CT angiogram performed at Stockport",
						"eventDetails" : "<ul><li>Philips PACS helps deliver GM’s goal of dynamic, high quality cross-enterprise reporting, as well as delivering unified image and report sharing throughout the GM collaborative</li><li>With the unique Philips Patient History Timeline users have access to all relevant clinical information, regardless of where it was acquired</li><li>The solution allows a radiologist at Stockport to highlight significant images to alert the vascular team by assigning them as Key Images</li><li>Philips PACS supports instant chat for collaboration</li><li>Philips PACS can support integration with Skype via its Application Programme Interface</li><li>The Philips Advanced Vessel Analysis delivers user-defined options to reduce comprehensive vascular planning time providing 3D visualisation of the vessels and centre lines</li><li>The vascular team at UHSM are able to access the images and all associated post processing</li></ul>",
						"eventIcon" : "icon_heart.png",
						"eventImage" : ""
					},
					{
						"eventTitle" : "PACS System Administrator reconciles temporary ID's",
						"eventDetails" : "<ul><li>Philips PACS has a web based administration tool that simplifies workflow for the management of temporary ID’s, duplicate patient ID’s and merges</li><li>Philips PACS is able to receive HL7-ADT messages from an authenticated source such as CRIS and/or PAS</li><li>When IntelliSpace PACS receives such a message it is able to be configured to:</li><ul><li>Automatically merge the two records within the PACS</li><li>Place the two records on a Link/Merge Candidates List within the administration tool for the System Administrator to action</li></ul><li>All activities affecting patient demographics, such as merges are recorded in an audit file</li></ul>",
						"eventIcon" : "icon_imaging.png",
						"eventImage" : ""
					}
				]
			}

//END
}
