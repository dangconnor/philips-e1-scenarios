// Philips Clinical Scenarios

	$(document).ready(function(){

		var windowH = 768;
		var windowW = 1024;

	setUpSlides = function(){

		var slideContainer = $("#container");

		var windowH = 768;
		var windowW = 1024;
		var startingPositions = [0];
		var totalWidth = 0;
		var goTo = 0;

		$("#super_container, #container, .slide, .slide-inside").css("height", windowH);
		$(".cover-slide").css("width", Math.round(windowW/3));

		$(".slide").each(function(){
			thisWidth = $(this).outerWidth();
			totalWidth += thisWidth;
			startingPositions.push(totalWidth);
		});

		slideContainer.width(totalWidth);

		slideContainer.css("left", 0);
		slidePositons = (startingPositions.length)-2;
		var lastSlidePos = -Math.abs(startingPositions[slidePositons]);

		slideContainer.draggable({
			axis: 'x',
		  	drag: function(event, ui) { 
		  		/*
					Callback
		  		*/
		  	}
		});

		var slideSlide = function(goTo){
			slideContainer.animate({
				left: goTo,
				}, 500, "easeInOutSine", function() {
					slideContainer.draggable({ disabled: false });
				}
			);
		}

		slideContainer.on( "dragstop", function( event, ui ) {

			slideContainer.draggable({ disabled: true }); 
			
				if (ui.position.left < lastSlidePos) {
					goTo = lastSlidePos;
					slideSlide(goTo);
				}else if (ui.position.left > 0) {
					goTo = 0;
					slideSlide(goTo);	
				}
				
			slideContainer.draggable({ disabled: false }); 

		});

		var slidesWidth = parseInt($("#container").outerWidth(true));
		var removeThis = parseInt($(".cover-slide").outerWidth(true));
		slidesWidth = slidesWidth-removeThis;
		$("#dasher").css({
			"width" : slidesWidth-150,
			"height" : "11.5px",
			"left" : removeThis,
			"top" : (windowH/2)-6
		});


		$("#super_container").css("opacity", 1);

	}

		var theKeys = Object.keys(scenariosObj);

		var superContainer = $("#super_container");

		// Create Footer Nav
		var footerScenarios = $("#footer-scenarios ul");
		var footerText = "";
		for (x in theKeys){
			footerScenarios.append("<li>"+theKeys[x]+"</li>");
		}

		howManyScenarios = theKeys.length;

		$("#footer-scenarios li").css("width", windowW/howManyScenarios);

		$("#footer-scenarios ul li").on('click', function(){
			$("#footer-scenarios ul li").removeClass("selected");
			scenario = $(this).html();
			$(this).addClass("selected");
			displayScenario(scenario);
		});
 
		// Function to read in scenariosObj
		var displayScenario = function(whichScenario){

			if(!whichScenario){
				whichScenario = theKeys[0];		
				$("#footer-scenarios ul li").first().addClass("selected");		
			}

			$("#super_container").css("opacity", 0);

			window.setTimeout(function() { 

							if($("#container").length){
								$("#container").remove();
							}

							$('<div/>', {id: 'container'}).prependTo(superContainer);

							theContainer = $("#container");
							theHeader = $("#header h1");

							var noOfEvents = scenariosObj[whichScenario].events.length;
							var lastThree = false;
							var topOrBottom = "top";

							/* From when we were stripping out the bracketted number 
							if (whichScenario.indexOf('(') > -1){
								whichScenarioNoBracket = whichScenario.substring(0, whichScenario.indexOf("("));
							}else{
								whichScenarioNoBracket = whichScenario;
							}
							*/
 							var whichScenarioCharOnly = whichScenario.charAt(0)
   

							theHeader.html("GM PACS Scenario "+whichScenarioCharOnly);

							// Dashed line
							theHTML = '<div id="dasher"><img src="images/Timeline_End_Bumper.png" /></div>';

							// Intro Slide
							theHTML += '<div class="slide cover-slide flex-container" id="intro-slide"><div><h2>Scenario '+whichScenarioCharOnly+'</h2><h3>'+scenariosObj[whichScenario].scenarioTitle+'</h3>';

							for(x in scenariosObj[whichScenario].specialistJobDescriptions){

								theHTML  += '<p>'+scenariosObj[whichScenario].specialistJobDescriptions[x].specialist+'</p>';
							}

							theHTML  += '</div></div>';

							// Event Slides
							for(x in scenariosObj[whichScenario].events){

								x = parseInt(x);
								thisSlide = x+1;
								if(thisSlide<10){
									thisSlide = "0"+thisSlide;
								}

								if((x+1)==(noOfEvents-2)){
									lastThree=true;
									theHTML += '<div class="slide">';
								}

								if(lastThree==true){
				            		amIInside = "-inside ";
								}else{
				            		amIInside = " ";
								}

								if((x+1)%2==0){
									titlePos="bottom";
									slideNoPos="top";
								}else{
									titlePos="top";
									slideNoPos="bottom";
								}

								theHTML += '<div class="slide'+amIInside+'flex-container flex-container-col"><p class="slide_text_'+titlePos+'">'+scenariosObj[whichScenario].events[x].eventTitle+'</p><img src="images/icons/'+scenariosObj[whichScenario].events[x].eventIcon+'" class="icon" data-sindex="'+x+'" data-scenario="'+whichScenario+
								'" /><p class="slideNo_'+slideNoPos+'">'+thisSlide+'</p><span class="event_title_'+titlePos+'"></span></div>';

				            	if(x<(noOfEvents-1)){
				                    theHTML += '<div class="slide'+amIInside+'flex-container flex-container-col-arrow"><img src="images/Timeline_Triangle.png" /></div>';
				            	}
							}

							theHTML += '</div>';

							theContainer.append(theHTML);

							$(".icon").on('click',function(){
								thisScenario = $(this).data("scenario");
								thisIndex = parseInt($(this).data("sindex"));
								thisIcon = scenariosObj[thisScenario].events[thisIndex].eventIcon;
								thisTitle = scenariosObj[thisScenario].events[thisIndex].eventTitle;
								theseDetails = scenariosObj[thisScenario].events[thisIndex].eventDetails;
								thisData = '<h2><img src="images/icons/'+thisIcon+'">'+thisTitle+'</h2>';
								thisData += theseDetails;
								$("#overlay .pop-up .pop-up-content").html(thisData);
								$("#overlay").css( { "display" : "block", "opacity" : 1});
							})

							$("#overlay #close-container").on('click',function(){
								$("#overlay").css( { "display" : "none", "opacity" : 0});
							})


							window.setTimeout(function() { 
								setUpSlides(); 
							}, 100);


			}, 500);

		

		}

		// init
		displayScenario();


	});